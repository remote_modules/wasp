cmake_minimum_required(VERSION 2.8)
project(NewTemporalSynthesis)

if(POLICY CMP0072)
    cmake_policy(SET CMP0072 NEW)
endif()

include(CTest)

find_package(OTB REQUIRED)
find_program(BASH_Program bash)

include(${OTB_USE_FILE})
include(FindPythonInterp)

#Include source directory to includes
INCLUDE_DIRECTORIES(
${PROJECT_SOURCE_DIR}/Code/Common/include
${PROJECT_SOURCE_DIR}/Code/LutReader/include
)

# Set a default build type if none was specified
set(default_build_type "Release")

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -Wno-missing-field-initializers")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})

find_package(Boost REQUIRED COMPONENTS unit_test_framework filesystem)
add_definitions(-DBOOST_TEST_DYN_LINK)

message("Project binary folder:" ${PROJECT_BINARY_DIR})
add_definitions(-DVCL_CAN_STATIC_CONST_INIT_FLOAT=0)


option(WASP_PYTHON_TESTS "Also run WASP Python tests" OFF)

set(WASP_INPUT_DATA "/WASP-Data-Input" CACHE PATH "input dir for testing")

set(OTB_INSTALL_APP_DIR "lib/otb/applications")
set(WASP_APP_BUILD_DIR "${CMAKE_BINARY_DIR}/lib/otb/applications")

# Configure the default WASP_BASELINE_ROOT for the location of OTB Data.
find_path(WASP_BASELINE_ROOT
  NAMES README-WASP-Baseline
  HINTS ${CMAKE_CURRENT_SOURCE_DIR}/Data
  )

if(WASP_BASELINE_ROOT)
    message(STATUS "WASP_BASELINE_ROOT: ${WASP_BASELINE_ROOT}")
endif()




add_subdirectory(Code/Common)
add_subdirectory(Code/MetadataHelper)
add_subdirectory(Code/MuscateMetadata)
add_subdirectory(Code/LutReader)
add_subdirectory(Code/CompositePreprocessing)
add_subdirectory(Code/WeightCalculation)
add_subdirectory(Code/UpdateSynthesis)
add_subdirectory(Code/ProductFormatter)
add_subdirectory(PythonScripts)

install(FILES ./Scripts/WASP  PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_WRITE GROUP_EXECUTE WORLD_EXECUTE DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

# first we can indicate the documentation build as an option and set it to ON by default
option(BUILD_DOC "Build documentation" ON)

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
add_custom_target(doc
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
COMMENT "Generating API documentation with Doxygen" VERBATIM
)
endif(DOXYGEN_FOUND)

message("Installing WASP to: " ${CMAKE_INSTALL_PREFIX})
