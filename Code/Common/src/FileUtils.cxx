/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../include/FileUtils.h"

#include "itksys/SystemTools.hxx"

void RelativeToAbsoluteFilename(const std::string & path, std::vector<std::string> & listOfFilename)
{
	const unsigned int nbFilenames(listOfFilename.size());
	for (unsigned int i = 0; i < nbFilenames; i++)
	{
		listOfFilename[i] = JoinFilenames(path, listOfFilename[i]);
	}
}

void CheckingExistenceFilenames(const std::vector<std::string> & listOfFilename)
{
	const unsigned int nbFilenames(listOfFilename.size());
	for (unsigned int i = 0; i < nbFilenames; i++)
	{
		CheckingExistenceFilename(listOfFilename[i]);
	}
}

std::string JoinFilenames(const std::string& filename1, const std::string& filename2)
{
	std::string relativePath(filename1);
	const std::string relativeName(filename2);
	if ((relativePath.size() && relativePath[relativePath.size() - 1] != '/') && (relativeName.size() && relativeName[0] != '/'))
	{
		relativePath += "/";
	}
	return relativePath + filename2;
}

void    CheckingExistenceFilename(const std::string & Filename)
{
	if (itksys::SystemTools::FileExists(Filename) == false)
	{
		throw std::runtime_error("Utilities::CheckingExtentFilenames: The filename " + Filename + " doesn't exist !");
	}
	if (itksys::SystemTools::FileIsDirectory(Filename) == true)
	{
		throw std::runtime_error(
				"Utilities::CheckingExtentFilenames: The filename " + Filename + " doesn't exist. It's a directory !");
	}
}
