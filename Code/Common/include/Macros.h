
/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "itkMacro.h"

/** Macro to define Set/GetInput method */
#define SetGetInputMacro(name,type,index) \
        /* ---------------------------------------------------------------------------------------------*/ \
        /* Set the ##name## Input image */ \
        void Set##name##Input( const type *object) \
        {\
            /* set the pointer */ \
            this->itk::ProcessObject::SetNthInput(index,const_cast<type*>(object)); \
        } \
        /* ---------------------------------------------------------------------------------------------*/ \
        /* Get the ##name## Input image */ \
        type * Get##name##Input() \
        { \
            /* return the pointer */ \
            return itkDynamicCastInDebugMode< type * >( this->itk::ProcessObject::GetInput(index) ); \
        } \
        /* ---------------------------------------------------------------------------------------------*/ \
        /* Get the ##name## Input image */ \
        const type * Get##name##Input() const \
        { \
            /* return the pointer */ \
            return itkDynamicCastInDebugMode< const type * >( this->itk::ProcessObject::GetInput(index) ); \
        }

/** Get a smart pointer to an object.  Creates the member
 * Get"name"() (e.g., GetPoints()). */
# define itkGetObjectMacro(name, type)                \
  virtual type * Get##name ()                         \
    {                                                 \
    return this->m_##name.GetPointer();               \
    }
