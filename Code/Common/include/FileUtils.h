
/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#ifndef COMMON_FILE_UTILS_HPP_
#define COMMON_FILE_UTILS_HPP_
#include <string>
#include <vector>

/**
 * @brief Split string by a char delimiter
 * @param s The string to be splitted
 * @param delim The delimiter, occuring [0...*]
 * @param remove_empty bool to remove empty values
 * @return A vector containing all the splitted substrings
 */
void RelativeToAbsoluteFilename(const std::string & path, std::vector<std::string> & listOfFilename);

/**
 * @brief Split string by a char delimiter
 * @param s The string to be splitted
 * @param delim The delimiter, occuring [0...*]
 * @param remove_empty bool to remove empty values
 * @return A vector containing all the splitted substrings
 */
void CheckingExistenceFilenames(const std::vector<std::string> & listOfFilename);

/**
 * @brief Split string by a char delimiter
 * @param s The string to be splitted
 * @param delim The delimiter, occuring [0...*]
 * @param remove_empty bool to remove empty values
 * @return A vector containing all the splitted substrings
 */
void CheckingExistenceFilename(const std::string & Filename);

/**
 * @brief Split string by a char delimiter
 * @param s The string to be splitted
 * @param delim The delimiter, occuring [0...*]
 * @param remove_empty bool to remove empty values
 * @return A vector containing all the splitted substrings
 */
void CheckingExistenceDirectory(const std::string & Filename);

/**
 * @brief Split string by a char delimiter
 * @param s The string to be splitted
 * @param delim The delimiter, occuring [0...*]
 * @param remove_empty bool to remove empty values
 * @return A vector containing all the splitted substrings
 */
std::string JoinFilenames(const std::string& filename1, const std::string& filename2);


#endif


