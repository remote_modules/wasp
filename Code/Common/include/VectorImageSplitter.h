/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Guillaume PASERO <guillaume.pasero@c-s.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef VECTOR_IMAGE_SPLITTER_H
#define VECTOR_IMAGE_SPLITTER_H

#include "otbVectorImageToImageListFilter.h"

namespace ts
{
/** \class VectorImageSplitter
 *  \brief This class aims at converting a multi-band image to a list of scalar images.
 *
 * This class take a VectorImage and converts it to an ImageList.
 *
 * It also handles properly the no-data flags if present.
 */
template <class TVectorImageType, class TImageList>
class ITK_EXPORT VectorImageSplitter
  : public otb::VectorImageToImageListFilter<TVectorImageType, TImageList>
{
public:
  /** Standard typedefs */
  typedef VectorImageSplitter Self;
  typedef otb::VectorImageToImageListFilter<TVectorImageType, TImageList> Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(VectorImageSplitter, VectorImageToImageListFilter);

  /** Template parameters typedefs */
  typedef TVectorImageType                        InputVectorImageType;
  typedef typename InputVectorImageType::Pointer  InputVectorImagePointerType;
  typedef TImageList                              OutputImageListType;
  typedef typename OutputImageListType::Pointer   OutputImageListPointerType;

  /** Generate the output information by building the output list. */
  void GenerateOutputInformation(void) override
    {
    Superclass::GenerateOutputInformation();

    OutputImageListPointerType  outputPtr = this->GetOutput();
    InputVectorImagePointerType inputPtr = this->GetInput();
  
    if (inputPtr)
      {
      std::vector<bool> noDataValueAvailable;
      bool ret = itk::ExposeMetaData<std::vector<bool> >(inputPtr->GetMetaDataDictionary(),otb::MetaDataKey::NoDataValueAvailable,noDataValueAvailable);
      std::vector<double> noDataValues;
      itk::ExposeMetaData<std::vector<double> >(inputPtr->GetMetaDataDictionary(),otb::MetaDataKey::NoDataValue,noDataValues);
      if (ret && noDataValueAvailable.size() >= inputPtr->GetNumberOfComponentsPerPixel())
        {
        // Dispatch the no-data values in each image of the list
        std::vector<bool> noDataFlagOut(1);
        std::vector<double> noDataValueOut(1);
        for (unsigned int i = 0; i < inputPtr->GetNumberOfComponentsPerPixel() ; ++i)
          {
          itk::MetaDataDictionary &dict = outputPtr->GetNthElement(i)->GetMetaDataDictionary();
          noDataFlagOut[0] = noDataValueAvailable[i];
          noDataValueOut[0] = noDataValues[i];
          itk::EncapsulateMetaData<std::vector<bool> >(dict,otb::MetaDataKey::NoDataValueAvailable, noDataFlagOut);
          itk::EncapsulateMetaData<std::vector<double> >(dict,otb::MetaDataKey::NoDataValue, noDataValueOut);
          }
        }
      }
    }

protected:

  /** Constructor */
  VectorImageSplitter() {};
  /** Destructor */
  ~VectorImageSplitter() override {}

private:
  VectorImageSplitter(const Self &) = delete;
  void operator =(const Self&) = delete;
};
} // End namespace ts

#endif
