/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Benjamin ESQUIS <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define BOOST_TEST_MODULE MuscateReader
#include <boost/test/unit_test.hpp>
#include "GlobalDefs.h"
#include "../include/vnsSimpleLutXMLFileHandler.h"
#include "../include/vnsVectorLookUpTable.h"
#include "../include/vnsLookUpTableFileReader.h"
#include "stdlib.h"
#include <string>
using namespace boost::unit_test;


#define TEST_NAME	"test_SimpleLutXMLReader"
#define EXAMPLE_LUTXML1 "giplut_dircor.xml"

const std::vector<double>  SolarZenithAngles = {15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80};
const std::vector<double>  ViewZenithAngles = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
const std::vector<double>  RelativeAzimuthIndex = {0,  10,  20,  30,  40,  50,  60,  70,  80,  90, 100, 110, 120, 130, 140, 150, 160, 170,  180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350};
const std::vector<double>  Value_Out = {0.0993027, 0.073317952454, 0.065475486, 0.0330186896, 0.05777016, 0.233487516, 0.3611008524, 0.380975335, 0.430103570222, 0.14473305642, 0.0648960843};

BOOST_AUTO_TEST_CASE(SimpleLUTXMLReaderExample1)
{
	BOOST_TEST_REQUIRE( framework::master_test_suite().argc == 2 );
	std::string TEST_SRC = framework::master_test_suite().argv[1];
	if(TEST_SRC == ""){
		BOOST_FAIL("Did not set WASP_INPUT_DATA cmake option!");
	}
	std::cout << std::string(TEST_SRC + "/" + TEST_NAME + "/" + EXAMPLE_LUTXML1) << std::endl;
	ts::SimpleLutXMLFileHandler::Pointer l_LUTXMLHandler = ts::SimpleLutXMLFileHandler::New();
	// Load the XML file and check with the schema
	l_LUTXMLHandler->LoadFile(std::string(TEST_SRC + "/" + TEST_NAME + "/" + EXAMPLE_LUTXML1));
	const ts::SimpleLutXMLContainer& l_lutxml = l_LUTXMLHandler->GetLutContainer();

	BOOST_REQUIRE(l_LUTXMLHandler);

	BOOST_CHECK_EQUAL(l_lutxml.GetSolar_Zenith_Angle_Indexes().size(), SolarZenithAngles.size());
	for(size_t i  = 0 ; i < l_lutxml.GetSolar_Zenith_Angle_Indexes().size();i++){
		BOOST_CHECK_EQUAL(l_lutxml.GetSolar_Zenith_Angle_Indexes()[i], SolarZenithAngles[i]);
	}
	BOOST_CHECK_EQUAL(l_lutxml.GetView_Zenith_Angle_Indexes().size(), ViewZenithAngles.size());
	for(size_t i  = 0 ; i < l_lutxml.GetView_Zenith_Angle_Indexes().size();i++){
		BOOST_CHECK_EQUAL(l_lutxml.GetView_Zenith_Angle_Indexes()[i], ViewZenithAngles[i]);
	}
	BOOST_CHECK_EQUAL(l_lutxml.GetRelative_Azimuth_Angle_Indexes().size(), RelativeAzimuthIndex.size());
		for(size_t i  = 0 ; i < l_lutxml.GetRelative_Azimuth_Angle_Indexes().size();i++){
			BOOST_CHECK_EQUAL(l_lutxml.GetRelative_Azimuth_Angle_Indexes()[i], RelativeAzimuthIndex[i]);
		}
	BOOST_CHECK_EQUAL(l_lutxml.GetListOfPackaged_DBL_Files().size(), 11);
}

BOOST_AUTO_TEST_CASE(SimpleLUTXMLReaderExample2)
{
	BOOST_TEST_REQUIRE( framework::master_test_suite().argc == 2 );
	std::string TEST_SRC = framework::master_test_suite().argv[1];
	if(TEST_SRC == ""){
		BOOST_FAIL("Did not set WASP_INPUT_DATA cmake option!");
	}
	std::cout << std::string(TEST_SRC + "/" + TEST_NAME + "/" + EXAMPLE_LUTXML1) << std::endl;
	ts::SimpleLutXMLFileHandler::Pointer l_LUTXMLHandler = ts::SimpleLutXMLFileHandler::New();
	// Load the XML file and check with the schema
	l_LUTXMLHandler->LoadFile(std::string(TEST_SRC + "/" + TEST_NAME + "/" + EXAMPLE_LUTXML1));
	const ts::SimpleLutXMLContainer& l_lutxml = l_LUTXMLHandler->GetLutContainer();

	BOOST_REQUIRE(l_LUTXMLHandler);

	//Reduced output lut typedef
	typedef ts::VNSLUT3DType LutType;
	typedef LutType::ConstPointer ReducedLutConstPointer;
	typedef LutType::Pointer ReducedLutPointer;
	//LookupTable Reader
	typedef ts::LookUpTableFileReader<LutType> LookUpTableReaderType;
	typedef LookUpTableReaderType::Pointer LookUpTableReaderPointer;
	typedef LutType::ParameterValuesType ParameterValuesType;
	// Init filters
	LookUpTableReaderPointer l_lookUpTableReader = LookUpTableReaderType::New();
	// Get the number of file that matches with the number of band
	const std::vector<std::string>& l_GIP_L2TOCR_ListOfFilenames = l_lutxml.GetListOfPackaged_DBL_Files();
	const unsigned int fileNumber = l_GIP_L2TOCR_ListOfFilenames.size();
	// For each spectral band, add the filename of the LUT to the reader
	for (unsigned int file = 0; file < fileNumber; file++)
	{
		// Set the filename of each band
		l_lookUpTableReader->AddBandFilename(l_GIP_L2TOCR_ListOfFilenames[file]);
		std::cout<<"BandFilename : " <<l_GIP_L2TOCR_ListOfFilenames[file]<<std::endl;
	}
	// Read informations from the GIPP file
	const std::vector<double>& l_GIP_L2TOCR_SolarZenithalAngleIndexes = l_lutxml.GetSolar_Zenith_Angle_Indexes();
	const std::vector<double>& l_GIP_L2TOCR_ViewZenithalAngleIndexes = l_lutxml.GetView_Zenith_Angle_Indexes();
	const std::vector<double>& l_GIP_L2TOCR_RelativeAzimuthAngleIndexes = l_lutxml.GetRelative_Azimuth_Angle_Indexes();

	/* parameters are added one by one to the LUT */
	ParameterValuesType l_TOCRParam1;
	l_TOCRParam1.ParameterName = "Solar_Zenith_Angle_Indexes";
	l_TOCRParam1.ParameterValues = l_GIP_L2TOCR_SolarZenithalAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam1);

	ParameterValuesType l_TOCRParam2;
	l_TOCRParam2.ParameterName = "View_Zenith_Angle_Indexes";
	l_TOCRParam2.ParameterValues = l_GIP_L2TOCR_ViewZenithalAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam2);

	ParameterValuesType l_TOCRParam3;
	l_TOCRParam3.ParameterName = "Relative_Azimuth_Angle_Indexes";
	l_TOCRParam3.ParameterValues = l_GIP_L2TOCR_RelativeAzimuthAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam3);

	//Gen Lut
	l_lookUpTableReader->GenerateLUT();

    typedef LutType::PointType LUTPointType;
    typedef LutType::Pointer LUTPointer;
    typedef LUTPointType::ValueType LUTPointValueType;
    typedef LutType::PixelType LUTPixelType;
    LUTPointer l_LutInPtr = l_lookUpTableReader->GetLUT();
	LUTPointType point;
	// Set the point[1] value
	point[0] = static_cast<LUTPointValueType>(15);
	point[1] = static_cast<LUTPointValueType>(8);
	point[2] = static_cast<LUTPointValueType>(100);

	LUTPixelType interpValue = l_LutInPtr->InterpolateValue(point);
	std::cout<<"Value : "<<interpValue<<std::endl;
	for (unsigned int idx = 0; idx < interpValue.Size(); idx++)
	{
		BOOST_CHECK_CLOSE(interpValue[idx], Value_Out[idx],0.00001);
	}

}


