set(MuscateMetadata_SOURCES
    src/MuscateMetadataReader.cxx
    src/MuscateMetadataWriter.cxx
    src/MuscateViewingAngles.cxx
    src/MuscateMetadataUtil.cxx)

add_library(MuscateMetadata SHARED ${MuscateMetadata_SOURCES})
target_link_libraries(MuscateMetadata
	Common
    "${Boost_LIBRARIES}"
    "${OTBCommon_LIBRARIES}"
    "${OTBTinyXML_LIBRARIES}"
    "${OTBITK_LIBRARIES}"    
    )

target_include_directories(MuscateMetadata PUBLIC include)

install(TARGETS MuscateMetadata DESTINATION lib/)

if(BUILD_TESTING)
  add_subdirectory(test)
endif()
