/*
 * Copyright (C) 2015-2016, CS Romania <office@c-s.ro>
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reversed
 *
 * This file is part of:
 * - Sen2agri-Processors (initial work)
 * - Weighted Average Synthesis Processor (WASP)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "BaseImageTypes.h"
#include "PreprocessingSentinel.h"
#include "PreprocessingVenus.h"

#include "otbChangeInformationImageFilter.h"

/**
 * @brief otb Namespace for all OTB-related Filters and Applications
 */
namespace otb
{
/**
 * @brief Wrapper namespace for all OTB-Applications
 */
namespace Wrapper
{

using namespace ts;

/**
 * @brief Perform the directional correction on all TOA-images.
 * First, the approximated NDVI is calculated using B4 and B8 of the Sentinel-2 products.
 * Afterwards, the Angles Rasters are created for each resolution and finally, the Water, Snow and Cloud-masks are read
 * to initialize the directional Correction
 * @note Runs on both Resolutions R1 and R2 sequentially, if both output filenames are set.
 */
class CompositePreprocessing : public Application, public BaseImageTypes
{
public:
	typedef CompositePreprocessing Self;
	typedef Application Superclass;
	typedef itk::SmartPointer<Self> Pointer;
	typedef itk::SmartPointer<const Self> ConstPointer;
	itkNewMacro(Self)
	itkTypeMacro(CompositePreprocessingOld, otb::Application)

	typedef otb::ChangeInformationImageFilter<FloatImageType> ChangeInfoFilterType;
	typedef otb::ChangeInformationImageFilter<Int16VectorImageType> ChangeInfoVectorFilterType;

private:

	/**
	 * @brief Inits the Documentation as well as the command-line parameters
	 */
	void DoInit()
	{
		SetName("CompositePreprocessing");
		SetDescription("Perform the directional correction of FRE Images");

		SetDocLongDescription("Perform the directional correction of FRE Images");
		SetDocLimitations("None");
		SetDocAuthors("Peter KETTIG");
		SetDocSeeAlso(" ");

		AddParameter(ParameterType_String, "xml", "Muscate L2A XML filepath");

		// Directional Correction parameters
		AddParameter(ParameterType_Choice, "dircor", "Directional correction mode");
		AddChoice("dircor.roy", "RoyCorrection");
		AddChoice("dircor.lut", "LutCorrection");
		MandatoryOff("dircor");
		AddParameter(ParameterType_String, "dircor.roy.scatteringcoeffsr1", "Scattering coefficients filename R1");
		MandatoryOff("dircor.roy.scatteringcoeffsr1");
		AddParameter(ParameterType_String, "dircor.roy.scatteringcoeffsr2", "Scattering coefficients filename R2");
		MandatoryOff("dircor.roy.scatteringcoeffsr2");
		AddParameter(ParameterType_String, "dircor.roy.scatteringcoeffsr3", "Scattering coefficients filename R3");
		MandatoryOff("dircor.roy.scatteringcoeffsr3");

		AddParameter(ParameterType_String, "dircor.lut.xmlfile", "Lut XML file description filename");
		// Output images parameters
		AddParameter(ParameterType_OutputImage, "outcld", "Out cloud mask image R1 resolution");
		MandatoryOff("outcld");
		AddParameter(ParameterType_OutputImage, "outwat", "Out water mask image R1 resolution");
		MandatoryOff("outwat");
		AddParameter(ParameterType_OutputImage, "outsnw", "Out snow mask image R1 resolution");
		MandatoryOff("outsnw");
		AddParameter(ParameterType_OutputImage, "outaot", "Out aot mask image R1 resolution");
		MandatoryOff("outaot");

		AddParameter(ParameterType_OutputImage, "outr1", "Out Image at R1 resolution");
		MandatoryOff("outr1");
		SetDefaultOutputPixelType("outr1", ImagePixelType_int16);
		AddParameter(ParameterType_OutputImage, "outr2", "Out Image at R2 resolution");
		MandatoryOff("outr2");
		SetDefaultOutputPixelType("outr2", ImagePixelType_int16);
		// Add outr3 for 60m process
		AddParameter(ParameterType_OutputImage, "outr3", "Out Image at R3 resolution");
		MandatoryOff("outr3");
		SetDefaultOutputPixelType("outr3", ImagePixelType_int16);


		AddRAMParameter();

		SetDocExampleParameterValue("xml", "/path/to/L2Aproduct_muscate.xml");
		SetDocExampleParameterValue("scatteringcoeffsr1", "/path/to/scattering_coeffs_10m.txt");
		SetDocExampleParameterValue("scatteringcoeffsr2", "/path/to/scattering_coeffs_20m.txt");

		SetDocExampleParameterValue("outr1", "/path/to/output_image_r1.tif");
		SetDocExampleParameterValue("outr2", "/path/to/output_image_r2.tif");
		SetDocExampleParameterValue("outcld", "/path/to/output_image_cloud.tif");
		SetDocExampleParameterValue("outwat", "/path/to/output_image_water.tif");
		SetDocExampleParameterValue("outsnw", "/path/to/output_image_snow.tif");
		SetDocExampleParameterValue("outaot", "/path/to/output_image_aot.tif");

	}

	/**
	 * @brief Updates the parameters
	 * @note Not needed here.
	 */
	void DoUpdateParameters()
	{
		if(this->HasValue("dircor.roy.scatteringcoeffsr1")&&this->HasValue("dircor.roy.scatteringcoeffsr3")){
		    otbAppLogFATAL("Parameter dircor.roy.scatteringcoeffsr1 (standard mode) can't be set with dircor.roy.scatteringcoeffsr3 (light mode)");
        }
        if(this->HasValue("outr1")&&this->HasValue("outr3")){
		    otbAppLogFATAL("Parameter outr1 (standard mode) can't be set with outr3 (light mode)");
        }
	}

	/**
	 * @brief Sets up and runs the pipeline
	 */
	void DoExecute()
	{
		std::string inXml = GetParameterAsString("xml");
		std::cout << "XML found: " << inXml << std::endl;
		auto factory = MetadataHelperFactory::New();
		auto pHelper = factory->GetMetadataHelper(inXml);
		m_processor = GetPreprocessor(pHelper->GetMissionName());
		m_processor->init();

		std::vector<bool> nd_flags;
		std::vector<double> nd_values;
		nd_flags.push_back(false);
		nd_values.push_back(0.0);
		m_MDSetterCP.clear();

		m_MDSetterCloud = ChangeInfoFilterType::New();
		// FloatImageType
		m_MDSetterCloud->SetInput(m_processor->getCloudMask(pHelper->GetCloudImageFileNames()[MAIN_RESOLUTION_INDEX]));
		m_MDSetterCloud->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
		FloatImageType::Pointer cldImg = m_MDSetterCloud->GetOutput();
		cldImg->UpdateOutputInformation();
		SetParameterOutputImage("outcld", cldImg.GetPointer());

		m_MDSetterWater = ChangeInfoFilterType::New();
		m_MDSetterWater->SetInput(m_processor->getWaterMask(pHelper->GetWaterImageFileNames()[MAIN_RESOLUTION_INDEX]));
		m_MDSetterWater->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
		FloatImageType::Pointer watImg = m_MDSetterWater->GetOutput();
		watImg->UpdateOutputInformation();
		SetParameterOutputImage("outwat", watImg.GetPointer());

		m_MDSetterSnow = ChangeInfoFilterType::New();
		m_MDSetterSnow->SetInput(m_processor->getSnowMask(pHelper->GetSnowImageFileNames()[MAIN_RESOLUTION_INDEX]));
		m_MDSetterSnow->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
		FloatImageType::Pointer snowImg = m_MDSetterSnow->GetOutput();
		snowImg->UpdateOutputInformation();
		SetParameterOutputImage("outsnw", snowImg.GetPointer());

		m_MDSetterAot = ChangeInfoFilterType::New();
		m_MDSetterAot->SetInput(m_processor->getAotMask(pHelper->GetAotImageFileNames()[MAIN_RESOLUTION_INDEX]));
		m_MDSetterAot->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
		FloatImageType::Pointer aotImg = m_MDSetterAot->GetOutput();
		aotImg->UpdateOutputInformation();
		SetParameterOutputImage("outaot", aotImg.GetPointer());

		size_t totalNRes = pHelper->getResolutions().getNumberOfResolutions();
		//Setup directional correction if asked
		if(this->HasValue("dircor.roy.scatteringcoeffsr1") && this->HasValue("dircor.roy.scatteringcoeffsr2")){
			std::vector<std::string> scatteringCoeffs = {GetParameterAsString("dircor.roy.scatteringcoeffsr1"),
					GetParameterAsString("dircor.roy.scatteringcoeffsr2")};
			m_processor->setDirCorMode(ROY);
			m_processor->setScatteringCoefficients(scatteringCoeffs);
		} else if(this->HasValue("dircor.roy.scatteringcoeffsr3")){
            std::vector<std::string> scatteringCoeffs = {GetParameterAsString("dircor.roy.scatteringcoeffsr3")};
			m_processor->setDirCorMode(ROY);
			m_processor->setScatteringCoefficients(scatteringCoeffs);
		} else if(this->HasValue("dircor.lut.xmlfile")){
			m_processor->setDirCorMode(LUT);
			m_processor->setDirCorLutXmlFileName(GetParameterAsString("dircor.lut.xmlfile"));
		}

		//Get the corrected rasters
		std::vector<Int16VectorImageType::Pointer> correctedRasters =
				m_processor->getCorrectedRasters(inXml, cldImg, watImg, snowImg);
		if(this->HasValue("outr3")){
            correctedRasters[0]->UpdateOutputInformation();
            nd_flags.assign(correctedRasters[0]->GetNumberOfComponentsPerPixel(), true);
            nd_values.assign(correctedRasters[0]->GetNumberOfComponentsPerPixel(), -10000.0);
            ChangeInfoVectorFilterType::Pointer mdSetter = ChangeInfoVectorFilterType::New();
            m_MDSetterCP.push_back(mdSetter);
            mdSetter->SetInput(correctedRasters[0]);
            mdSetter->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
            mdSetter->SetOutputMetaData<std::vector<double> >(otb::MetaDataKey::NoDataValue,&nd_values);
            SetParameterOutputImage(std::string("outr3"), mdSetter->GetOutput());
		}
		else{
		    //For all possible resolutions, do
            for(size_t resolution = 0; resolution < totalNRes; resolution++){
                correctedRasters[resolution]->UpdateOutputInformation();
                nd_flags.assign(correctedRasters[resolution]->GetNumberOfComponentsPerPixel(), true);
                nd_values.assign(correctedRasters[resolution]->GetNumberOfComponentsPerPixel(), -10000.0);
                ChangeInfoVectorFilterType::Pointer mdSetter = ChangeInfoVectorFilterType::New();
                m_MDSetterCP.push_back(mdSetter);
                mdSetter->SetInput(correctedRasters[resolution]);
                mdSetter->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&nd_flags);
                mdSetter->SetOutputMetaData<std::vector<double> >(otb::MetaDataKey::NoDataValue,&nd_values);
                SetParameterOutputImage(std::string("outr" + std::to_string(resolution+1)).c_str(), mdSetter->GetOutput());
            }
            std::string parameterStr = "outr2";
            std::string parameter = GetParameterAsString(parameterStr);
            if(!parameter.empty() && totalNRes < size_t(N_RESOLUTIONS_SENTINEL)){
                itkExceptionMacro("Cannot set parameter " << parameterStr << " for a Platform with less than 2 resolutions");
            }

        }

	}

private:

	/**
	 * @brief Get the preprocessor depending on the platform
	 * @param p The Platform string, which can be: SENTINEL, VENUS
	 * @return Unique pointer to the Preprocessor which was chosen
	 */
	std::unique_ptr<preprocessing::PreprocessingAdapter> GetPreprocessor(const std::string& p){

		if(std::string(p).find(SENTINEL_MISSION_STR) != std::string::npos){
			std::unique_ptr<preprocessing::PreprocessingAdapter> sentinelMuscate(new preprocessing::PreprocessingSentinel);
			return sentinelMuscate;
		}
		if(std::string(p).find(VENUS_MISSION_STR) != std::string::npos){
			std::unique_ptr<preprocessing::PreprocessingAdapter> venusMuscate(new preprocessing::PreprocessingVenus);
			return venusMuscate;
		}
		itkExceptionMacro("Cannot find PreprocessingAdapter for the given platform: " << p);
	}


	/////////////////////////
	/// Private Variables //
	///////////////////////

	std::unique_ptr<preprocessing::PreprocessingAdapter> m_processor;

	ChangeInfoFilterType::Pointer m_MDSetterCloud;
	ChangeInfoFilterType::Pointer m_MDSetterWater;
	ChangeInfoFilterType::Pointer m_MDSetterSnow;
	ChangeInfoFilterType::Pointer m_MDSetterAot;

	std::vector<ChangeInfoVectorFilterType::Pointer> m_MDSetterCP;

};

} //namespace Wrapper
} //namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::CompositePreprocessing)



