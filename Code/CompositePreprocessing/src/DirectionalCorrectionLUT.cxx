/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "DirectionalCorrectionLUT.h"
#include "MetadataHelperFactory.h"
#include <vector>

using namespace ts;

DirectionalCorrectionLUT::DirectionalCorrectionLUT() {
	m_nRes = 0;
	m_strXml = "";
	m_strLutFileName = "";
}

void DirectionalCorrectionLUT::Init(const size_t &res, const std::string &xml,
		const std::string &lut_file,FloatImageType::Pointer &cldImg, FloatImageType::Pointer &watImg,
		FloatImageType::Pointer &snowImg, FloatVectorImageType::Pointer &angles) {
	m_nRes = res;
	m_strXml = xml;
	m_strLutFileName = lut_file;
	m_AnglesImg = angles;
	m_CSM = cldImg;
	m_WM = watImg;
	m_SM = snowImg;

	m_ImageList = ImageListType::New();
	m_AngleImageList = ImageListType::New();
	m_Concat = ListConcatenerFilterType::New();
	m_ConcatAngle = ListConcatenerFilterType::New();
	m_ReaderList = FloatVectorImageReaderListType::New();
}

void DirectionalCorrectionLUT::DoExecute() {
	//Load the reflectances files
	auto factory = ts::MetadataHelperFactory::New();
	auto pHelper = factory->GetMetadataHelper(m_strXml);
	ts::productReturnType inputImageFiles = pHelper->getResolutions().getNthResolutionFilenames(m_nRes);
	std::cout << "Directional Correction Filenames found: " << std::endl;
	for(auto filename : inputImageFiles){
		FloatVectorImageReaderType::Pointer reader = FloatVectorImageReaderType::New();
		reader->SetFileName(filename);
		std::cout << filename << std::endl;
		reader->UpdateOutputInformation();
		m_ReaderList->PushBack(reader);
		FloatVectorImageType::Pointer inputImg = reader->GetOutput();
		extractBandsFromImage(inputImg, m_ImageList);
	}

	//Construct the band list
	std::vector<Band> l_bandList = pHelper->getResolutions().getResolutionVector()[m_nRes].getBands();

	//Read the xml lut description file
	SimpleLutXMLFileHandler::Pointer l_LUTXMLHandler = ts::SimpleLutXMLFileHandler::New();
	// Load the XML file and check with the schema
	l_LUTXMLHandler->LoadFile(m_strLutFileName);
	const ts::SimpleLutXMLContainer& l_lutxml = l_LUTXMLHandler->GetLutContainer();

	// Init LUT filters
	LookUpTableReaderPointer l_lookUpTableReader = LookUpTableReaderType::New();
	// Get the number of file that matches with the number of band
	const std::vector<std::string>& l_GIP_DIRCOR_ListOfFilenames = l_lutxml.GetListOfPackaged_DBL_Files();
	const unsigned int fileNumber = l_GIP_DIRCOR_ListOfFilenames.size();

	for (auto it : l_bandList)
	{
		// For each spectral band, add the filename of the LUT to the reader
		for (auto file : l_GIP_DIRCOR_ListOfFilenames)
		{
			if (file.find(it.getType()) != std::string::npos)
			{
				std::cout<<"LUT file found for band "<<it.getType()<<" : "<<file<<std::endl;
				l_lookUpTableReader->AddBandFilename(file);
			}
		}
	}
	// Read informations from the GIPP file
	const std::vector<double>& l_GIP_DIRCOR_SolarZenithalAngleIndexes = l_lutxml.GetSolar_Zenith_Angle_Indexes();
	const std::vector<double>& l_GIP_DIRCOR_ViewZenithalAngleIndexes = l_lutxml.GetView_Zenith_Angle_Indexes();
	const std::vector<double>& l_GIP_DIRCOR_RelativeAzimuthAngleIndexes = l_lutxml.GetRelative_Azimuth_Angle_Indexes();

	/* parameters are added one by one to the LUT */
	ParameterValuesType l_TOCRParam1;
	l_TOCRParam1.ParameterName = "Solar_Zenith_Angle_Indexes";
	l_TOCRParam1.ParameterValues = l_GIP_DIRCOR_SolarZenithalAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam1);

	ParameterValuesType l_TOCRParam2;
	l_TOCRParam2.ParameterName = "View_Zenith_Angle_Indexes";
	l_TOCRParam2.ParameterValues = l_GIP_DIRCOR_ViewZenithalAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam2);

	ParameterValuesType l_TOCRParam3;
	l_TOCRParam3.ParameterName = "Relative_Azimuth_Angle_Indexes";
	l_TOCRParam3.ParameterValues = l_GIP_DIRCOR_RelativeAzimuthAngleIndexes;
	l_lookUpTableReader->AddParameterValues(l_TOCRParam3);
	//Gen Lut
	l_lookUpTableReader->GenerateLUT();
	//Useful typedef for LUT
	typedef LutType::PointType LUTPointType;
	typedef LutType::Pointer LUTPointer;
	//Get the lut pointer
	LUTPointer l_LutInPtr = l_lookUpTableReader->GetLUT();

	//Init concat image pipeline
	m_Concat->SetInput(m_ImageList);
	m_AnglesImg->UpdateOutputInformation();
	//Init concat angles pipeline
	extractBandsFromImage(m_AnglesImg,m_AngleImageList);
	m_ConcatAngle->SetInput(m_AngleImageList);
	m_ConcatAngle->UpdateOutputInformation();
	m_Concat->UpdateOutputInformation();
	m_CSM->UpdateOutputInformation();
	m_SM->UpdateOutputInformation();
	m_WM->UpdateOutputInformation();

	//Setup directional filter
	m_DirectionalCorrectionFilter = DirectionalCorrectionFilterType::New();
	m_DirectionalCorrectionFilter->SetAnglesInput(m_ConcatAngle->GetOutput());
	m_DirectionalCorrectionFilter->SetReflInput(m_Concat->GetOutput());
	m_DirectionalCorrectionFilter->SetCloudMaskInput(m_CSM);
	m_DirectionalCorrectionFilter->SetSnowMaskInput(m_SM);
	m_DirectionalCorrectionFilter->SetWaterMaskInput(m_WM);
	m_DirectionalCorrectionFilter->SetLUT(l_LutInPtr);
	m_DirectionalCorrectionFilter->UpdateOutputInformation();

}

int DirectionalCorrectionLUT::extractBandsFromImage(FloatVectorImageType::Pointer & imageType, ImageListType::Pointer & listToFill) {
	imageType->UpdateOutputInformation();
	int nbBands = imageType->GetNumberOfComponentsPerPixel();
	for(int j=0; j < nbBands; j++)
	{
		// extract the band without resampling
		listToFill->PushBack(m_ResampledBandsExtractor.ExtractImgResampledBand(imageType, j+1, ts::Interpolator_Linear));
	}
	return nbBands;
}

DirectionalCorrectionLUT::ShortVectorImageType::Pointer DirectionalCorrectionLUT::GetCorrectedImg() {
	return m_DirectionalCorrectionFilter->GetOutput();
}

DirectionalCorrectionLUT::FloatVectorImageType::Pointer DirectionalCorrectionLUT::GetCoeffImg() {
	return m_DirectionalCorrectionFilter->GetCoeffImage();
}
