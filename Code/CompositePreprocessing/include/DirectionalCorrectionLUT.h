/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DIRECTIONAL_CORRECTIONLUT_H
#define DIRECTIONAL_CORRECTIONLUT_H

#include <DirectionalCorrectionLUTFilter.h>
#include "otbImage.h"
#include "otbVectorImage.h"
#include "otbImageList.h"
#include "BaseImageTypes.h"
#include "otbImageListToVectorImageFilter.h"
#include "otbFunctorImageFilter.h"
#include "ResamplingBandExtractor.h"
#include "vnsVectorLookUpTable.h"
#include "vnsLookUpTableFileReader.h"
#include "vnsSimpleLutXMLFileHandler.h"

/**
 * @brief The TemporalSynthesis namespace, covering all needed functions to execute this processing chain
 */
namespace ts {

/**
 * @brief Perform the directional correction on a set of images of the same resolution
 */
class DirectionalCorrectionLUT : public BaseImageTypes {
public:

	typedef otb::ImageList<FloatImageType>												ImageListType;
	typedef otb::ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>		ListConcatenerFilterType;

	typedef DirectionalCorrectionLUTFilter<FloatVectorImageType,FloatVectorImageType,FloatImageType,FloatVectorImageType,ShortVectorImageType>	DirectionalCorrectionFilterType;
	typedef DirectionalCorrectionFilterType::Pointer	DirectionalCorrectionFilterPointer;

    //Reduced output lut typedef
    typedef VNSLUT3DType LutType;
    typedef typename LutType::ConstPointer ReducedLutConstPointer;
    typedef typename LutType::Pointer ReducedLutPointer;
    //LookupTable Reader
    typedef LookUpTableFileReader<LutType> LookUpTableReaderType;
    typedef typename LookUpTableReaderType::Pointer LookUpTableReaderPointer;
    typedef typename LutType::ParameterValuesType ParameterValuesType;

public:
	/**
	 * @brief Constructor
	 */
	DirectionalCorrectionLUT();

	/**
	 * @brief Init the Directional Correction
	 * @param res Current resolution
	 * @param xml Metadata file
	 * @param scatcoef Scattering coefficient filename
	 * @param cldImg Cloud Image
	 * @param watImg Water Image
	 * @param snowImg Snow Image
	 * @param angles Angles Image
	 * @param ndvi NDVI Image
	 */
	void Init(const size_t &res, const std::string &xml, const std::string &lut_file, FloatImageType::Pointer &cldImg,
			FloatImageType::Pointer &watImg, FloatImageType::Pointer &snowImg,
			FloatVectorImageType::Pointer &angles);

	/**
	 * @brief Execute the application
	 */
	void DoExecute();

	/**
	 * @brief Return the corrected image
	 * @return ShortVectorImage containing the corrected S2-rasters
	 */
	ShortVectorImageType::Pointer GetCorrectedImg();

	/**
	 * @brief Return the corrected image
	 * @return ShortVectorImage containing the corrected S2-rasters
	 */
	FloatVectorImageType::Pointer GetCoeffImg();

	/**
	 * @brief Return the name of the class
	 * @return
	 */
	const char * GetNameOfClass() { return "DirectionalCorrection"; }

private:
	/**
	 * @brief Extract each band from the angles image to the image list
	 * @param imageType FloatVectorImage of the angles image
	 * @return The number of bands extracted
	 */
	int extractBandsFromImage(FloatVectorImageType::Pointer & imageType, ImageListType::Pointer & listToFill);

private:
	size_t                                  	m_nRes;
	std::string                            		m_strXml;
	std::string                            		m_strLutFileName;
	FloatVectorImageType::Pointer               m_AnglesImg;
	FloatImageType::Pointer                		m_CSM, m_WM, m_SM;
	ImageListType::Pointer                  	m_ImageList;
	ImageListType::Pointer                  	m_AngleImageList;
	ListConcatenerFilterType::Pointer       	m_Concat;
	ListConcatenerFilterType::Pointer       	m_ConcatAngle;
	DirectionalCorrectionFilterPointer        	m_DirectionalCorrectionFilter;

	FloatVectorImageReaderType::Pointer         m_inputImageReader;
	FloatVectorImageReaderListType::Pointer		m_ReaderList;
	ts::ResamplingBandExtractor<float>          m_ResampledBandsExtractor;
};
} //namespace ts
#endif // DIRECTIONAL_CORRECTIONLUT_H
