/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DIRECTIONALCORRECTIONLUTFILTER_H
#define DIRECTIONALCORRECTIONLUTFILTER_H

#include "GlobalDefs.h"
#include "Utils.h"
#include "Macros.h"
#include "itkImageToImageFilter.h"

#include "../../LutReader/include/vnsVectorLookUpTable.h"


/**
 * @brief The TemporalSynthesis namespace, covering all needed functions to execute this processing chain
 */
namespace ts {

template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
class DirectionalCorrectionLUTFilter : public itk::ImageToImageFilter<TInputReflectanceImage, TOutput>
{
public:
    /** Standard class typedefs. */
    typedef DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput> Self;
    typedef itk::ImageToImageFilter<TInputReflectanceImage, TOutput> Superclass;
    typedef itk::SmartPointer<Self> Pointer;
    typedef itk::SmartPointer<const Self> ConstPointer;

    /** Type macro */
    itkNewMacro(Self)

    /** Creation through object factory macro */
    itkTypeMacro(DirectionalCorrectionLUTFilter, ProcessObject)

	typedef TInputReflectanceImage InputReflImageType;
    typedef typename InputReflImageType::Pointer InputReflImagePointer;
    typedef typename InputReflImageType::ConstPointer InputReflImageConstPointer;
    typedef typename InputReflImageType::PixelType InputReflImagePixelType;
	typedef TInputAngleImage InputAngleImageType;
	typedef typename InputAngleImageType::Pointer InputAngleImagePointer;
	typedef typename InputAngleImageType::ConstPointer InputAngleImageConstPointer;
	typedef typename InputAngleImageType::PixelType InputAngleImagePixelType;
	typedef TInputMask InputMaskType;
	typedef typename InputMaskType::Pointer InputMaskPointer;
	typedef typename InputMaskType::ConstPointer InputMaskConstPointer;
	typedef typename InputMaskType::PixelType InputMaskPixelType;
    /** Output image typedefs. */
    typedef TOutput OutputImageType;
    typedef typename OutputImageType::Pointer OutputImagePointer;
    typedef typename OutputImageType::ConstPointer OutputImageConstPointer;
    typedef typename OutputImageType::PixelType OutputImagePixelType;
    typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;
    typedef typename OutputImageType::RegionType OutputImageRegionType;
    /** Output coeff image typedefs. */
    typedef TOuputCoeffImage OutputCoeffImageType;
    typedef typename OutputCoeffImageType::Pointer OutputCoeffImagePointer;
    typedef typename OutputCoeffImageType::ConstPointer OutputCoeffImageConstPointer;
    typedef typename OutputCoeffImageType::PixelType OutputCoeffImagePixelType;
    typedef typename OutputCoeffImageType::InternalPixelType OutputCoeffImageInternalPixelType;
    typedef typename OutputCoeffImageType::RegionType OutputCoeffImageRegionType;
    /** LUT image typedef */
    typedef VNSLUT3DType LUTType;
    typedef typename LUTType::Pointer LUTPointer;
    typedef typename LUTType::ConstPointer LUTConstPointer;
    typedef typename LUTType::PixelType LUTPixelType;
    typedef typename LUTType::InternalPixelType LUTInternalPixelType;
    typedef typename LUTType::PointType LUTPointType;
    typedef typename LUTPointType::ValueType LUTPointValueType;

    /** Product julian date accessors */
    itkSetMacro(fReflNoDataValue, float)
    itkGetConstReferenceMacro(fReflNoDataValue, float)


	/** Set the TOAC reflectance image */
	SetGetInputMacro(SnowMask , InputMaskType, 0);
	/** Set the AOT reflectance image */
	SetGetInputMacro(WaterMask, InputMaskType, 1);
	/** Set the EDG image */
	SetGetInputMacro(CloudMask, InputMaskType, 2);
	/** Set the DTM image */
	SetGetInputMacro(Refl , InputReflImageType, 3);
	/** Set the DTM image */
	SetGetInputMacro(Angles , InputAngleImageType, 4);
    /** Set the LUT image */
    itkSetObjectMacro(LUT, LUTType)
    /** Get the DTM image */
    itkGetConstObjectMacro(LUT, LUTType)


	/** Get the coefficients image */
	OutputCoeffImageType * GetCoeffImage();

protected:
    /** Constructor */
	DirectionalCorrectionLUTFilter();
    /** Destructor */
    virtual
    ~DirectionalCorrectionLUTFilter();

    void
    operator=(const Self&); //purposely not implemented

    /** Validate the presence of all inputs and their sizes. If at least one input
     * is missing, throws an exception. */
    void
    CheckInputs();

    /** BeforeThreaded generate data. */
    virtual void
    BeforeThreadedGenerateData(void);

    /** Generate output information. */
    virtual void
    GenerateOutputInformation(void);

    /** Composite Filter method. */
    virtual void
    ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);

private:
	float   m_fReflNoDataValue;
    /** Surface reflectance LUT */
	LUTConstPointer m_LUT;

};

} //namespace ts

#include "DirectionalCorrectionLUTFilter.hxx"

#endif // DIRECTIONALCORRECTIONFUNCTOR_H
