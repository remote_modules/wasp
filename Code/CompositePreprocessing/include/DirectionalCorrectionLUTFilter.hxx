/*
 * Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
 * All rights reserved
 *
 * This file is part of Weighted Average Synthesis Processor (WASP)
 *
 * Authors:
 * - Esquis Benjamin <benjamin.esquis@csgroup.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * See the LICENSE.md file for more details.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "DirectionalCorrectionLUTFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"
#include "GlobalDefs.h"


/**
 * @brief The TemporalSynthesis namespace, covering all needed functions to execute this processing chain
 */
namespace ts {


// Constructor
template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask, TOuputCoeffImage, TOutput>::DirectionalCorrectionLUTFilter()
{
	// Input/Output initialization
	this->SetNumberOfIndexedInputs(5);
	this->SetNumberOfRequiredInputs(5);
	this->SetNumberOfIndexedOutputs(2);

	this->SetNthOutput(0, OutputImageType::New());
	this->SetNthOutput(1, OutputCoeffImageType::New());
	
	m_fReflNoDataValue = NO_DATA_VALUE;
};

// Destructor
template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::~DirectionalCorrectionLUTFilter()
{
}

template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
typename DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::OutputCoeffImageType *
DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::GetCoeffImage(void)
{
	return itkDynamicCastInDebugMode<OutputCoeffImageType *>(itk::ProcessObject::GetOutput(1));
}

//===============  Generate output information method ===================/
template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
void DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::GenerateOutputInformation()
{
	// Call superclass implementation
	Superclass::GenerateOutputInformation();

	// Set the number of output channels to the number of input one.
	// Check Snow Mask	
	if (!this->GetSnowMaskInput())
	{
		throw new std::runtime_error("Waiting for a SnowMask image, but no one specified. Please set one.");
	}
	// Check water mask availability	
	if (!this->GetWaterMaskInput())
	{
		throw new std::runtime_error("Waiting for a water mask, but no one specified. Please set one.");
	}
	// Check cloud mask availability	
	if (!this->GetCloudMaskInput())
	{
		throw new std::runtime_error("Waiting for a cloud mask, but no one specified. Please set one.");
	}
	// Check cloud mask availability	
	if (!this->GetReflInput())
	{
		throw new std::runtime_error("Waiting for a reflectance image, but no one specified. Please set one.");
	}
	// Check cloud mask availability
	if (!this->GetAnglesInput())
		{
		throw new std::runtime_error("Waiting for an Angles image, but no one specified. Please set one.");
	}	
	this->GetOutput()->SetNumberOfComponentsPerPixel(this->GetReflInput()->GetNumberOfComponentsPerPixel());
	this->GetCoeffImage()->SetNumberOfComponentsPerPixel(this->GetReflInput()->GetNumberOfComponentsPerPixel());
}

//=============== BeforeThreaded generate data method ================/
template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
void DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::BeforeThreadedGenerateData()
{
	// Call superclass implementation
	Superclass::BeforeThreadedGenerateData();
	
	// Check LUT availability
	if (m_LUT.IsNull() == true)
	{
		throw new std::runtime_error("Waiting for a LUT, but no one specified. Please set one.");
	}

	// Check Snow Mask
	if (!this->GetSnowMaskInput())
	{
		throw new std::runtime_error("Waiting for a SnowMask image, but no one specified. Please set one.");
	}
	// Check water mask availability
	if (!this->GetWaterMaskInput())
	{
		throw new std::runtime_error("Waiting for a water mask, but no one specified. Please set one.");
	}
	// Check cloud mask availability
	if (!this->GetCloudMaskInput())
	{
		throw new std::runtime_error("Waiting for a cloud mask, but no one specified. Please set one.");
	}
	// Check cloud mask availability
	if (!this->GetReflInput())
	{
		throw new std::runtime_error("Waiting for a reflectance image, but no one specified. Please set one.");
	}
	// Check cloud mask availability
	if (!this->GetAnglesInput())
	{
		throw new std::runtime_error("Waiting for an Angles image, but no one specified. Please set one.");
	}

}

//====================== Main computation method ======================/
template<class TInputAngleImage, class TInputReflectanceImage,class TInputMask, class TOuputCoeffImage, class TOutput>
void DirectionalCorrectionLUTFilter<TInputAngleImage, TInputReflectanceImage,TInputMask,TOuputCoeffImage, TOutput>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType /*threadId*/)
{
	// Load inputs
	// Input availability are checked in the BeforeThreadedGenerateData.
	InputReflImageConstPointer l_IPReflPtr = this->GetReflInput();
	InputAngleImageConstPointer l_IPAnglePtr = this->GetAnglesInput();
	InputMaskConstPointer l_IPCLDPtr = this->GetCloudMaskInput();
	InputMaskConstPointer l_IPSNWPtr = this->GetSnowMaskInput();
	InputMaskConstPointer l_IPWMPtr = this->GetWaterMaskInput();


	// Load outputs
	OutputImagePointer l_OPReflPtr = this->GetOutput();
	// Coeff image
	OutputCoeffImagePointer l_OCoeffPtr = this->GetCoeffImage();
	// Prepare the threaded region input local iterators
	itk::ImageRegionConstIterator<InputReflImageType> l_IPReflIt(l_IPReflPtr, outputRegionForThread);
	itk::ImageRegionConstIterator<InputAngleImageType> l_IPAngleIt(l_IPAnglePtr, outputRegionForThread);
	itk::ImageRegionConstIterator<InputMaskType> l_IPCLDIt(l_IPCLDPtr, outputRegionForThread);
	itk::ImageRegionConstIterator<InputMaskType> l_IPSNWIt(l_IPSNWPtr, outputRegionForThread);
	itk::ImageRegionConstIterator<InputMaskType> l_IPWMIt(l_IPWMPtr, outputRegionForThread);

	// Init to the beginning
	l_IPReflIt.GoToBegin();
	l_IPAngleIt.GoToBegin();
	l_IPCLDIt.GoToBegin();
	l_IPSNWIt.GoToBegin();
	l_IPWMIt.GoToBegin();

	// Prepare the threaded region output local iterator
	itk::ImageRegionIterator<OutputImageType> l_OPReflIt(l_OPReflPtr, outputRegionForThread);
	itk::ImageRegionIterator<OutputCoeffImageType> l_OCoeffIt(l_OCoeffPtr, outputRegionForThread);

	// Init to the beginning
	l_OPReflIt.GoToBegin();
	l_OCoeffIt.GoToBegin();

	// Get the number of band of the input image
	const unsigned int nbOfChannels(l_IPReflPtr->GetNumberOfComponentsPerPixel());

	// Set the output default pixel value to NoData.
	OutputImagePixelType l_OPReflPix;
	l_OPReflPix.SetSize(nbOfChannels);
	OutputCoeffImagePixelType l_OCoeffPix;
	l_OCoeffPix.SetSize(nbOfChannels);
	//Lut ExtractPoint
	LUTPointType point;
	LUTPointType pointRef;
	pointRef.Fill(0);

	// Loop over input and output images
	while ((l_OPReflIt.IsAtEnd() == false) && (l_IPReflIt.IsAtEnd() == false))
	{
		const double thetaS = l_IPAngleIt.Get()[0];
		const double phiS = l_IPAngleIt.Get()[1];
		point[0] = thetaS;
		pointRef[0] = thetaS;
		LUTPixelType interpRefValue = m_LUT->InterpolateValue(pointRef);
		//Fill coeff pixel with 1.0 factor
		l_OCoeffPix.Fill(1.0);
		
		const InputReflImagePixelType& l_inRefl = l_IPReflIt.Get();
		// Loop over the channels		
		const bool inCloud = ( (int)l_IPCLDIt.Get() != 0);
		const bool inWater = ( (int)l_IPWMIt.Get() != 0);
		const bool inSnow = ( (int)l_IPSNWIt.Get() != 0);
		// If the pixel is not a No_data pixel
		if ( inCloud || inWater || inSnow )
		{
			// Band loop
			for (unsigned int i = 0; i < nbOfChannels; i++)
			{
				l_OPReflPix[i] = static_cast<OutputImageInternalPixelType>(l_inRefl[i]);
				l_OCoeffPix[i] = -4;
			}
		} else
		{	
			// Band loop
			for (unsigned int i = 0; i < nbOfChannels; i++)
			{				
				if(compareValueWithEpsilon<float>(l_inRefl[i], m_fReflNoDataValue)) {
					l_OPReflPix[i] = static_cast<OutputImageInternalPixelType>(m_fReflNoDataValue);
					l_OCoeffPix[i] = -1;
				} else {
					const double thetaV = l_IPAngleIt.Get()[2*i+1];
					point[1] = thetaV;
					const double phiV = l_IPAngleIt.Get()[2*i+2];
					if(std::isnan(thetaV) || std::isnan(phiV)) {
						l_OPReflPix[i] = static_cast<OutputImageInternalPixelType>(l_inRefl[i]);
						l_OCoeffPix[i] = -2;
					} else {
						// Get the relative azimuth angle
						double deltaPhi = phiS - phiV;
						if (deltaPhi < 0.)
						{
							deltaPhi = -deltaPhi;
						}
						if (deltaPhi > 180.)
						{
							deltaPhi = 360 - deltaPhi;
						}
						point[2] = deltaPhi;						
						LUTPixelType interpValue = m_LUT->InterpolateValue(point);
						const float l_dirNorm = interpRefValue[i] / interpValue[i];
						l_OCoeffPix[i] = static_cast<OutputCoeffImageInternalPixelType>(l_dirNorm);
						const float fNewReflVal = l_inRefl[i] * l_dirNorm;
						if(fNewReflVal < 0) {
							l_OPReflPix[i] = static_cast<OutputImageInternalPixelType>(l_inRefl[i]);
							l_OCoeffPix[i] = -3;
						} else {
							l_OPReflPix[i] =  static_cast<OutputImageInternalPixelType>(fNewReflVal);
						}
					}
				}
			}
		}
		// Fill the output pixel iterator
		l_OPReflIt.Set(l_OPReflPix);
		l_OCoeffIt.Set(l_OCoeffPix);

		++l_OPReflIt;
		++l_OCoeffIt;
		++l_IPReflIt;
		++l_IPAngleIt;
		++l_IPCLDIt;
		++l_IPSNWIt;
		++l_IPWMIt;

	} // end pixel loop

}

} //namespace ts


