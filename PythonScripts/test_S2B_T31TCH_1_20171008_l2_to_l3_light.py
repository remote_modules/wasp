#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (C) 2018-2019, Centre National d'Etudes Spatiales (CNES)
All rights reserved

This file is part of Weighted Average Synthesis Processor (WASP)

Authors:
- Peter KETTIG <peter.kettig@cnes.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

See the LICENSE.md file for more details.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import unittest
import os
import WASP
import CompareL3AProducts
from base_comparison import BaseComparison
import sys
import pathlib
import shutil

class T31TCH_20171008_l2_to_l3_light(unittest.TestCase, BaseComparison):
    test_name = "test_S2B_T31TCH_1_20171008_l2_to_l3_light"
    out_path = os.path.join("SENTINEL2B_20171008-105012-463_L3A_T31TCH_C_V1-0",
                                "SENTINEL2B_20171008-105012-463_L3A_T31TCH_C_V1-0_MTD_ALL.xml")
    inputs = [os.path.join("SENTINEL2B_20171008-105012-463_L2A_T31TCH_C_V1-0",
                                  "SENTINEL2B_20171008-105012-463_L2A_T31TCH_C_V1-0_MTD_ALL.xml")]

    def setUp(self):
        self.setupEnvironment()

    def test_run(self):
        input_path = [os.path.join(self.wasp_test_path,
                                  self.test_name, "INPUTS", i) for i in self.inputs]
        print('[%s]' % ', '.join(map(str, input_path)))
        for xml in input_path:
            if(not os.path.exists(xml)):
                print("Cannot find %s".format(xml))
        out = self.execPath
        out = os.path.join(self.wasp_test_path, self.test_name)
        ts = WASP.TemporalSynthesis(self.createArgs(input_path, out, lightmode="True", bands="B4,B8,B11,B12"))
        ts.run()
        out_new = os.path.join(out, self.out_path)
        out_golden = os.path.join(self.wasp_baseline_path, self.test_name,
                                  self.out_path)
        self.assertTrue(os.path.exists(out_new))
        self.assertTrue(os.path.exists(out_golden))

        comparator = CompareL3AProducts.Comparator(out_golden, out_new)
        self.assertTrue(comparator.run())

        light_product_path = pathlib.Path(self.wasp_test_path) / "test_S2B_T31TCH_1_20171008_l2_to_l3_light"\
                             / "SENTINEL2B_20171008-105012-463_L2A_T31TCH_C_V1-0_LIGHT"
        shutil.rmtree(light_product_path)
        return


if __name__ == '__main__':
    if len(sys.argv) > 1:        
        T31TCH_20171008_l2_to_l3_light.wasp_baseline_path = sys.argv.pop()
        T31TCH_20171008_l2_to_l3_light.wasp_test_path = sys.argv.pop()
    unittest.main()
