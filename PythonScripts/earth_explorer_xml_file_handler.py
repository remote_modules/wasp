# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     |||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler -- shortdesc

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler is a description

It defines classes_and_methods

###################################################################################################
"""
import xml_tools as xml_tools
import os,logging

from lxml import etree as et


EARTH_EXPLORER_HANDLER_XPATH = {
    "Mission": "//Earth_Explorer_Header/Fixed_Header/Mission",
    "FileName": "//Earth_Explorer_Header/Fixed_Header/File_Name",
    "DBLFilenames": "//Earth_Explorer_Header/Variable_Header/Specific_Product_Header/DBL_Organization/" +
                    "List_of_Packaged_DBL_Files/Packaged_DBL_File/Relative_File_Path",
    "ListOfBands": "//List_of_Bands",
    "band_list_theor": "//List_of_Band_Theoretical_Wavelength",
    "band_theorical_wavelength_sub": "//Band_Theoretical_Wavelength",
    "ValidityStart": "//Fixed_Header/Validity_Period/Validity_Start",
    "ValidityStop": "//Fixed_Header/Validity_Period/Validity_Stop",
    "SignificantBits": "//Number_of_Significant_Bits",
    "File_Type": "//Fixed_Header/File_Type",
    "Creator": "//Creator",
    "CreatorVersion": "//Creator_Version",
    "OrbitNumber": "//SENSING_ORBIT_NUMBER",
    "ReferenceSystemCode": "//HORIZONTAL_CS_CODE",
    "ReferenceSystemShortDescription": "//HORIZONTAL_CS_NAME",
    "GenerationTime": "//GENERATION_TIME",
    "ProductStartTime" : "//PRODUCT_START_TIME",
    "ProductURI" : "//PRODUCT_URI",
    "SamplingFactor" : "//Earth_Explorer_Header/Variable_Header/Specific_Product_Header/Annex_Information/Subsampling_Factor/By_Line"
}


class EarthExplorerXMLFileHandler(object):

    EE_SCHEMA_DECIMAL_9_FORMAT = "%.9f"

    XML_FORMAT_DATE = "UTC=%Y-%m-%dT%H:%M:%S"

    class PackagedDBLFileType:
        def __init__(self):
            self.relativefilepath = ""
            self.filedefinition = ""

    def __init__(self, main_xml_file):
        """
        Constructor
        """
        logging.debug("Loading " + main_xml_file)
        self.main_xml_file = main_xml_file
        self.root = xml_tools.get_root_xml(self.main_xml_file, deannotate=True)
        self.orig_root = xml_tools.get_root_xml(self.main_xml_file, deannotate=False)
        self.nss = self.root.nsmap        

    def save_to_file(self, output_filename):
        self.orig_root[:] = self.root[:]
        xml_tools.save_to_xml_file(self.orig_root, output_filename)
        os.system(
            'XMLLINT_INDENT="    "  xmllint --format ' + str(output_filename) + " --output " + str(output_filename))

    def get_string_value_of(self, key):
        return xml_tools.get_only_value(self.root, EARTH_EXPLORER_HANDLER_XPATH[key], namespaces=self.nss).text

    def get_string_value(self, xpath):
        return xml_tools.get_only_value(self.root, xpath, namespaces=self.nss).text

    def replace_node(self, node_src, xpath, check_if_present=False):
        xml_tools.replace_node(node_src, xpath, self.root, xpath, check_if_present)

    def set_string_value(self, xpath, value, check_if_present=True):
        xml_tools.set_value(self.root, xpath, value, check=check_if_present)

    def get_list_of_packaged_dbl_files(self, absolute, check):
        nodes = xml_tools.get_all_values(self.root, EARTH_EXPLORER_HANDLER_XPATH["DBLFilenames"])
        list_of_files = [nd.text for nd in nodes]
        if absolute:
            l_rootbasepath = os.path.dirname(self.main_xml_file)
            list_of_files = [os.path.join(l_rootbasepath, nd.text) for nd in nodes]
        if check:
            for fi in list_of_files:
                if not os.path.exists(fi):
                    raise Exception("File " + fi + " does not exists")
        return list_of_files
