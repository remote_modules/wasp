"""
    Set of functions to manage the xml of different levels product
"""
import subprocess

from lxml import etree

BAND_GLOBAL_LIST = "//Product_Characteristics/Band_Global_List"
BAND_GROUP_LIST = "//Product_Characteristics/Band_Group_List"
IMAGE_LIST = "//Product_Organisation/Muscate_Product/Image_List"
IMAGE_FILE_LIST = "//Product_Organisation/Muscate_Product/Image_List/Image/Image_File_List"
MASK_LIST = "//Product_Organisation/Muscate_Product/Mask_List"
GROUP_GEOPOSITIONING_LIST = "//Geopositioning/Group_Geopositioning_List"
SPECTRAL_BAND_LIST = "//Radiometric_Informations/Spectral_Band_Informations_List"
VIEWING_INCIDENCE_ANGLE_LIST = "//Geometric_Informations/Angles_Grids_List/Viewing_Incidence_Angles_Grids_List"
MEAN_VIEWING_INCIDENCE_ANGLE_LIST = "//Geometric_Informations/Mean_Value_List/Mean_Viewing_Incidence_Angle_List"

MASK_DICT = {"FLG": "Pixel_Status_Flag", "WGT": "Pixel_Weight", "DTS": "Weighted_Average_Dates"}
MASK_LIST_DICT = {"IAB": ["AOT_Interpolation", "WVC_Interpolation"], "DFP": ["Defective_Pixel"],
                  "CLM": ["Detailed_Cloud"], "DTF": ["Detector_Footprint"], "EDG": ["Edge"], "SAT": ["Saturation"],
                  "MG2": ["Cloud", "Cloud_Shadow", "Hidden_Surface", "Snow", "Sun_Too_Low", "Tangent_Sun",
                          "Topography_Shadow", "Water"]}


def get_unique_val_xpath(node, xpath_expr):
    """
        Get the unique value of the xpath expression
        If no result or many results found raise exception
    """
    result = node.xpath(xpath_expr)
    if not result:
        raise Exception("No result found with the expression %s", xpath_expr)
    elif len(result) > 1:
        raise Exception("Several results found with the expression %s", xpath_expr)
    else:
        return result[0]


def get_val_xpath(node, xpath_expr):
    """
        Get the values of the xpath expression
    """
    return node.xpath(xpath_expr)


def delete_bands_from_global_list(root, bands_to_keep):
    node = get_unique_val_xpath(root, BAND_GLOBAL_LIST)
    for child in get_val_xpath(node, './BAND_ID'):
        if child.text not in bands_to_keep:
            child.getparent().remove(child)
    node.set('count', str(len(node.getchildren())))


def delete_and_add_bands_from_group_list(root, bands_to_keep):
    """
        Delete R1 and R2 groups and create R3 group with bands to keep from group list node
    """
    node = get_unique_val_xpath(root, BAND_GROUP_LIST)
    r1_node = get_unique_val_xpath(node, "./Group[@group_id='R1']")
    r2_node = get_unique_val_xpath(node, "./Group[@group_id='R2']")
    r1_node.getparent().remove(r1_node)
    r2_node.getparent().remove(r2_node)

    r3_node = etree.SubElement(node, "Group")
    r3_node.set("group_id", "R3")

    band_list_node = etree.SubElement(r3_node, "Band_List")
    band_list_node.set("count", str(len(bands_to_keep)))

    for band in bands_to_keep:
        band_node = etree.SubElement(band_list_node, "BAND_ID")
        band_node.text = band


def manage_aot(root, keep_aot):
    """
        Manage ATB image for Product_Organisation node, contains Aerosol Optical Thickness and Water Vapor Content
    """
    node = get_unique_val_xpath(root, IMAGE_LIST)
    aot = get_unique_val_xpath(node, "./Image/Image_Properties/NATURE[text()='Aerosol_Optical_Thickness']/../..")
    wpc = get_unique_val_xpath(node, "./Image/Image_Properties/NATURE[text()='Water_Vapor_Content']/../..")
    bands_list = [aot, wpc]
    for b in bands_list:
        r1_node = get_unique_val_xpath(b, "./Image_File_List/IMAGE_FILE[@group_id='R1']")
        r1_node.getparent().remove(r1_node)
        r2_node = get_unique_val_xpath(b, "./Image_File_List/IMAGE_FILE[@group_id='R2']")
        if keep_aot:
            r2_node.set("group_id", "R3")
            r2_node.text = r2_node.text[:-6] + "R3.tif"
        else:
            r2_node.getparent().remove(r2_node)


def manage_fre_sre(root, bands_to_keep):
    """
        Manage FRE and SRE images for Product_Organisation node, remove SRE images and keep FRE images of bands_to_keep
    """
    node = get_unique_val_xpath(root, IMAGE_LIST)
    fre_node = get_unique_val_xpath(node, "./Image/Image_Properties/NATURE[text()='Flat_Reflectance']/../..")
    sre_node = get_val_xpath(node, "./Image/Image_Properties/NATURE[text()='Surface_Reflectance']/../..")
    if len(sre_node):
        sre_node[0].getparent().remove(sre_node[0])
    for child in get_val_xpath(fre_node, './Image_File_List/IMAGE_FILE'):
        if child.get('band_id') not in bands_to_keep:
            child.getparent().remove(child)


def remove_viewing_incidence_angles(root, bands_to_keep):
    node = get_unique_val_xpath(root, VIEWING_INCIDENCE_ANGLE_LIST)
    for child in get_val_xpath(node, './Band_Viewing_Incidence_Angles_Grids_List'):
        if child.get('band_id') not in bands_to_keep:
            child.getparent().remove(child)


def remove_mean_viewing_incidence_angles(root, bands_to_keep):
    node = get_unique_val_xpath(root, MEAN_VIEWING_INCIDENCE_ANGLE_LIST)
    for child in get_val_xpath(node, './Mean_Viewing_Incidence_Angle'):
        if child.get('band_id') not in bands_to_keep:
            child.getparent().remove(child)


def manage_mask(root, mask_name, keep=True):
    node = get_unique_val_xpath(root, MASK_LIST)
    for m in MASK_LIST_DICT[mask_name]:
        if not get_val_xpath(node, f"./Mask/Mask_Properties/NATURE[text()='{m}']/../.."):
            continue
        mask_node = get_unique_val_xpath(node, f"./Mask/Mask_Properties/NATURE[text()='{m}']/../..")
        if not keep:
            mask_node.getparent().remove(mask_node)
            continue
        # For MLG2 and CLM
        # Modification to be made if other masks are to be kept
        if "group_id" in get_val_xpath(mask_node, "./Mask_File_List/MASK_FILE")[0].keys():
            r1_node = get_unique_val_xpath(mask_node, "./Mask_File_List/MASK_FILE[@group_id='R1']")
            r1_node.getparent().remove(r1_node)
            r2_node = get_unique_val_xpath(mask_node, "./Mask_File_List/MASK_FILE[@group_id='R2']")
            r2_node.set("group_id", "R3")
            r2_node.text = r2_node.text[:-6] + "R3.tif"


def product_organisation(root, bands_to_keep, masks_to_keep, keep_aot):
    # IMAGES
    manage_aot(root, keep_aot)
    manage_fre_sre(root, bands_to_keep)

    # MASKS
    manage_mask(root, "IAB", "IAB" in masks_to_keep)
    manage_mask(root, "DFP", "DFP" in masks_to_keep)
    manage_mask(root, "CLM", "CLM" in masks_to_keep)
    manage_mask(root, "DTF", "DTF" in masks_to_keep)
    manage_mask(root, "EDG", "EDG" in masks_to_keep)
    manage_mask(root, "SAT", "SAT" in masks_to_keep)
    manage_mask(root, "MG2", "MG2" in masks_to_keep)


def delete_bands_from_spectral_band_list(root, bands_to_keep):
    node = get_unique_val_xpath(root, SPECTRAL_BAND_LIST)
    for child in get_val_xpath(node, './Spectral_Band_Informations'):
        if child.get("band_id") not in bands_to_keep:
            child.getparent().remove(child)
        else:
            get_unique_val_xpath(child, "./SPATIAL_RESOLUTION").text = "60"


def change_node_from_group_geopositioning_list(root):
    node = get_unique_val_xpath(root, GROUP_GEOPOSITIONING_LIST)
    r1_node = get_unique_val_xpath(node, "Group_Geopositioning[@group_id='R1']")
    r1_node.getparent().remove(r1_node)
    r2_node = get_unique_val_xpath(node, "Group_Geopositioning[@group_id='R2']")
    r2_node.set("group_id", "R3")
    if "-" in get_unique_val_xpath(r2_node, "./XDIM").text:
        get_unique_val_xpath(r2_node, "./XDIM").text = str(-60)
    else:
        get_unique_val_xpath(r2_node, "./XDIM").text = str(60)
    if "-" in get_unique_val_xpath(r2_node, "./YDIM").text:
        get_unique_val_xpath(r2_node, "./YDIM").text = str(-60)
    else:
        get_unique_val_xpath(r2_node, "./YDIM").text = str(60)
    get_unique_val_xpath(r2_node, "./NROWS").text = str(1830)
    get_unique_val_xpath(r2_node, "./NCOLS").text = str(1830)


def get_product_resolutions(xml):
    root_tree = open_xml(xml)
    root = root_tree.getroot()
    node = get_unique_val_xpath(root, BAND_GROUP_LIST)
    return [child.get("group_id") for child in node.xpath("./Group")]


def get_bands_from_global_list(xml):
    root_tree = open_xml(xml)
    root = root_tree.getroot()
    bands_list = []
    node = get_unique_val_xpath(root, BAND_GLOBAL_LIST)
    for child in get_val_xpath(node, './BAND_ID'):
        bands_list.append(child.text)
    return bands_list


def check_image_list(xml):
    root_tree = open_xml(xml)
    root = root_tree.getroot()
    node = get_val_xpath(root, IMAGE_LIST)
    if len(node) == 0:
        return False
    return True

def open_xml(path):
    return etree.parse(path)


def save_xml(root_tree, path):
    root_tree.write(path)


def create_new_xml_for_l3_light(in_path, out_path, bands_to_keep, masks_to_keep, keep_aot):
    if not bands_to_keep:
        bands_to_keep = ["B2", "B3", "B4", "B5", "B6", "B7", "B8", "B8A", "B11", "B12"]
    root_tree = open_xml(in_path)
    root = root_tree.getroot()
    delete_bands_from_global_list(root, bands_to_keep)
    delete_and_add_bands_from_group_list(root, bands_to_keep)
    product_organisation(root, bands_to_keep, masks_to_keep, keep_aot)
    change_node_from_group_geopositioning_list(root)
    delete_bands_from_spectral_band_list(root, bands_to_keep)
    remove_viewing_incidence_angles(root, bands_to_keep)
    remove_mean_viewing_incidence_angles(root, bands_to_keep)
    save_xml(root_tree, out_path)


def check_xml(xml_path, xsd_path):
    args = f"xmllint {xml_path} --schema {xsd_path} --noout"
    args = args.split(" ")
    subprocess.run(args)
