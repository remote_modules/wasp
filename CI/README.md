# Continuous Integration for WASP

This document presents the CI implemented for WASP.

## Platforms tests

WASP is tested on 2 platforms (64bits):

* CentOS 7, with compiler GCC 7
* Ubuntu 20.04 LTS, with compiler GCC 9

These environments are defined as `Dockerfiles` located in `CI/RH7` and `CI/Ubuntu`.

## Test Data

The test data has been shrunk with the script `crop_s2.sh`. It fills a large part
of the input images with zeros (or the adequate no-data value), leaving only the
original data on a small strip. The output images are then stored in GeoTIFF with
DEFLATE compression to save space.

The test data is split between 2 places:

* [S3 bucket WASP-Data](https://s3.orfeo-toolbox.org/minio/wasp-data/) : contains
  the input data for tests, the shrunk times series of S2 and Venus
* Git repository under `Data`: contains the baselines, stored as LFS objects.

## Pipelines

This is the overall structure of the CI pipeline:

```mermaid
graph LR;
  subgraph "stage:Container"
  A["CentOS container"];
  B["Ubuntu container"];
  end
  subgraph "stage:Build"
  C["CentOS build"];
  D["Ubuntu build"];
  end
  A-->C
  B-->D
  subgraph "stage::Validation"
  C-->E["CentOS validation"];
  end
  subgraph "stage:Quality"
  E-->F["Quality report"];
  end
  D-->F
  subgraph "stage:Delivery"
  G["Docker image delivery"]
  end
  C-->G
```

There are some specificites regarding the branches. Some jobs are not run for
every branch.

| Branch       | Container          | Build              | Validation         | Quality            | Delivery           |
|--------------|--------------------|--------------------|--------------------|--------------------|--------------------|
| master       | :white_check_mark: | :white_check_mark: | :white_check_mark: |                    |                    |
| develop      | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |                    |
| release-vX.Y | :white_check_mark: | :white_check_mark: | :white_check_mark: |                    | :white_check_mark: |
| ci           | :white_check_mark: | :white_check_mark: | :white_check_mark: |                    | :white_check_mark: |
| *others*     |                    | :white_check_mark: | :white_check_mark: |                    |                    |

If the container is built on a given branch, the same container is used in
the following jobs of the pipeline. When no container is built in the pipeline,
the container from `develop` is used. 

### Container

We use Kaniko to build base environments for the CI (CentOS 7 and Ubuntu 20.04).
The docker images are uploaded to the
[registry](https://gitlab.orfeo-toolbox.org/remote_modules/wasp/container_registry)
with the following names:

* `wasp-env-centos7`
* `wasp-env-ubuntu`

They are tagged with the branch name.

### Build

On CentOS 7, the steps are:

* Build WASP in `Release` mode
* Run C++ unit tests
* Create `.run` package

On Ubuntu, the steps are:

* Build WASP in `Debug` mode, with `--coverage` option
* Run C++ unit tests
* Generate C++ coverage report with `gcov` and `gcovr`
* Convert Test report from CTest to Junit format
* Run `cppcheck` for C++ code analysis

This job produces the reports (test, coverage, code analysis) for the C++ side
of WASP.

### Validation

Only run on CentOS 7:

* Install the `.run` package from build stage
* Run Python validation tests for the whole chain, with `nosetests` and coverage options
* Run `pylint` for Python code analysis

This job produces the reports (test, coverage, code analysis) for the Python side
of WASP.

### Quality

Run on the Ubuntu container that contains `sonar-scanner`:

* Send all quality reports to [SonarQube](https://sonar.orfeo-toolbox.org/dashboard?id=wasp).

### Delivery

Produce the official Docker image, and uploads it ti the
[registry](https://gitlab.orfeo-toolbox.org/remote_modules/wasp/container_registry) 
with the following name : wasp.

## Future improvements

There are some points that can be improved:

* The C++ unit tests don't cover the applications in `WeightCalculation` and
  `UpdateSynthesis`, so the coverage is not measured for these folders.
* The tests using directional LUT are disabled, they can be enabled when the
  input data and baselines are uploaded.
* The Python tests run are `test_S2B_T31TCH_1_20171008` and `test_VNS_FR-LUS_4_20180207`.
  Even with shrunk products, the whole chain is a bit long to run. Optimizations
  should be performed before enabling the other Python tests. Ideally, the pipeline
  should run in less than 30min.

