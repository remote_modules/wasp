#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Usage : $0  input_dir  output_dir"
  exit 1
fi

dump_md()
{
gdalinfo $1 > dump.txt
}

get_pixel_type()
{
gdalType=$(cat dump.txt | grep '^Band 1 ' | grep -oE 'Type=[^,]+' | cut -d '=' -f 2)
case "$gdalType" in
  "Byte")
    echo -n uint8
    ;;
  "Int16")
    echo -n int16
    ;;
  "UInt16")
    echo -n uint16
    ;;
  "UInt32")
    echo -n uint32
    ;;
  "Int32")
    echo -n int32
    ;;
  "Float32")
    echo -n float
    ;;
  "Float64")
    echo -n double
    ;;
  "CInt16")
    echo -n cint16
    ;;
  "CInt32")
    echo -n cint32
    ;;
  "CFloat32")
    echo -n cfloat
    ;;
  "CFloat64")
    echo -n cdouble
    ;;
  *)
    echo -n unknown
    ;;
esac
}

# get decimation factor for s2 file
get_s2_sub_factor()
{
gdalSize=$(cat dump.txt | grep '^Size is ' | grep -oE '[0-9]+' | head -n 1)
case $gdalSize in
  10980)
    echo -n 1
    ;;
  5490)
    echo -n 2
    ;;
  *)
    echo -n 0
    ;;
esac
}

# get no data fill value for current file
get_nodata_val()
{
NAME=$1
if [ "$(echo $NAME | grep -E '_(SRE|FRE|FRC)_[A-Z0-9]*\.tif')" ]; then
  echo -n -10000
elif [ "$(echo $NAME | grep -E '_ATB_(R[0-9]|XS)\.tif')" ]; then
  echo -n 0
elif [ "$(echo $NAME | grep -E '_EDG_(R[0-9]|XS)\.tif')" ]; then
  echo -n 1
elif [ "$(echo $NAME | grep -E '_IAB_(R[0-9]|XS)\.tif')" ]; then
  echo -n 3
elif [ "$(echo $NAME | grep -E '_CP_(R[0-9]|XS)\.tif')" ]; then
  echo -n -10000
elif [ "$(echo $NAME | grep -E '^s2angles_raster_.*\.tif')" ]; then
  echo -n -10000
elif [ "$(echo $NAME | grep -E '_(DTS|WGT)_(R[0-9]|XS)\.tif')" ]; then
  echo -n -10000
else
  # default value 0 for all other cases
  echo -n 0
fi
}

get_sensor_name()
{
NAME=$1
if [ "$(echo $NAME | grep '^SENTINEL2')" ]; then
  echo -n S2
elif [ "$(echo $NAME | grep '^VENUS')" ]; then
  echo -n VNS
else
  echo -n UNKNOWN
fi
}

reset_margin()
{
INPUT_FILE=$1
OUTPUT_DIR=$2
SENSOR=$3
FILENAME="$(basename $INPUT_FILE)"
OUTPUT_FILE="$OUTPUT_DIR/$FILENAME"
VAL="$(get_nodata_val $FILENAME)"

dump_md $INPUT_FILE

pixType=$(get_pixel_type)
if [ "$SENSOR" == "S2" ]; then
  subFactor=$(get_s2_sub_factor)
elif [ "$SENSOR" == "VNS" ]; then
  subFactor=1
fi

LINE_START=$(eval "echo \${${SENSOR}_LINE_START} / $subFactor | bc")
LINE_SIZE=$(eval "echo \$${SENSOR}_LINE_SIZE / $subFactor | bc")

COL_START=$(eval "echo \${${SENSOR}_COL_START} / $subFactor | bc")
COL_SIZE=$(eval "echo \$${SENSOR}_COL_SIZE / $subFactor | bc")
#~ echo "$LINE_START by $LINE_SIZE"

#~ echo "$FILENAME has $pixType pixel type"

otbApplicationLauncherCommandLine ResetMargin2 $HOME/WASP/ResetMargin \
  -in $INPUT_FILE \
  -out "$OUTPUT_FILE?gdal:co:COMPRESS=$COMPRESS&gdal:co:BLOCKYSIZE=$BLOCKYSIZE" $pixType \
  -roi.startx $COL_START \
  -roi.sizex $COL_SIZE \
  -roi.starty $LINE_START \
  -roi.sizey $LINE_SIZE \
  -val $VAL \
  -progress 0
}

crop_product()
{
inProd=$1
outProd="$2/$(basename $inProd)"
mkdir -p $outProd

echo Cropping product $inProd

cp $inProd/*.xml $outProd
cp $inProd/*.jpg $outProd

prodDirName=$(basename $inProd)
sensorShortName=$(get_sensor_name $prodDirName)

for name in $(ls $inProd/{VENUS,SENTINEL2}*.tif 2>/dev/null); do
  reset_margin $name $outProd $sensorShortName
done

if [ -d "$inProd/MASKS" ]; then
  mkdir -p $outProd/MASKS
  for name in $(ls $inProd/MASKS/{VENUS,SENTINEL2}*.tif 2>/dev/null); do
    reset_margin $name $outProd/MASKS $sensorShortName
  done
fi

if [ -d "$inProd/DATA" ]; then
  mkdir -p $outProd/DATA
  cp -r $inProd/DATA/* $outProd/DATA
fi

}

crop_test_case()
{
inTC=$1
outTC="$2/$(basename $inTC)"
mkdir -p $outTC

for name in $(ls -d $inTC/* 2>/dev/null); do
  name2=$(echo "$name" | grep -E "$MUSCATE_REGEX")
  if [ -d "$name2" ]; then
    crop_product $name $outTC
  fi
done

if [ ! -d "$inTC/INPUTS" ]; then
  return
fi
mkdir -p $outTC/INPUTS

cp $inTC/INPUTS/*.txt $outTC/INPUTS 2>/dev/null

for name in $(ls $inTC/INPUTS/*.tif 2>/dev/null); do
  # only expect S2 products in this case
  reset_margin $name $outTC/INPUTS S2
done

for name in $(ls -d $inTC/INPUTS/* 2>/dev/null); do
  name2=$(echo "$name" | grep -E "$MUSCATE_REGEX")
  if [ -d "$name2" ]; then
    crop_product $name $outTC/INPUTS
  fi
done

}

inDir=$1
outDir=$2

# GDAL params
COMPRESS=DEFLATE
BLOCKYSIZE=8
# cropping params (expressed at R1)
S2_LINE_START=2048
S2_LINE_SIZE=16
S2_COL_START=0
S2_COL_SIZE=99999
# cropping params (expressed at XS)
VNS_LINE_START=2048
VNS_LINE_SIZE=16
VNS_COL_START=0
VNS_COL_SIZE=99999
# Regex to search L2A and L3A products
MUSCATE_REGEX="([A-Z0-9-]+_){2}L[2-3]A(_[A-Z0-9-]+){3}\$"

mkdir -p $outDir

# init env
source $HOME/otb-release/7.2.0/otbenv.profile

# test_VNS_FR-LUS_4_20180207
crop_test_case $inDir/test_VNS_FR-LUS_4_20180207 $outDir

# test_Preprocessing
crop_test_case $inDir/test_Preprocessing $outDir

# test_S2A_T33UUU_8_20150727
crop_test_case $inDir/test_S2A_T33UUU_8_20150727 $outDir

# test_S2B_T31TCH_1_20171008
crop_test_case $inDir/test_S2B_T31TCH_1_20171008 $outDir

# test_S2B_T31TGL_1_20180321
crop_test_case $inDir/test_S2B_T31TGL_1_20180321 $outDir

# test_S2X_T31TCJ_11_20180511
crop_test_case $inDir/test_S2X_T31TCJ_11_20180511 $outDir

