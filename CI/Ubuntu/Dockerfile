# ----------------------------------------------------------------------------
# Multi-stage build: Stage #1 (CppCheck build)
# ----------------------------------------------------------------------------

FROM ubuntu:20.04 as builder

ENV DEBIAN_FRONTEND noninteractive
ENV CPPCHECK_VERSION 2.7

RUN echo "Europe/Paris" > /etc/timezone

RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
        curl \
        ca-certificates \
        tar \
        g++ \
        gcc \
        gzip \
        libpcre3-dev \
        make \
        python \
 && rm -rf /var/lib/apt/lists/*

# Install up-to-date CppCheck version
RUN cd /tmp \
    && curl -L -o cppcheck-${CPPCHECK_VERSION}.tar.gz \
         https://github.com/danmar/cppcheck/archive/${CPPCHECK_VERSION}.tar.gz \
    && tar xzf cppcheck-${CPPCHECK_VERSION}.tar.gz \
    && cd cppcheck-${CPPCHECK_VERSION} \
    && make SRCDIR=build FILESDIR=/usr/share/cppcheck/ HAVE_RULES=yes \
            CXXFLAGS="-O2 -DNDEBUG -Wall -Wno-sign-compare -Wno-unused-function"

# ----------------------------------------------------------------------------
# Multi-stage build: Stage #2 (main build)
# ----------------------------------------------------------------------------

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
ENV CPPCHECK_VERSION 2.7
ENV SONAR_SCANNER_VERSION 4.7.0.2747

RUN echo "Europe/Paris" > /etc/timezone

RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
        bzip2 \
        ca-certificates \
        ccache \
        cmake \
        curl \
        doxygen \
        file \
        g++ \
        gcc \
        gcovr \
        git \
        libglvnd-dev \
        libglu1-mesa-dev \
        libpcre3 \
        libpython3-dev \
        libsaxon-java \
        libtool \
        make \
        ninja-build \
        openjdk-11-jre-headless \
        patch \
        patchelf \
        python3 \
        python3-pip \
        rclone \
        unzip \
        wget \
 && rm -rf /var/lib/apt/lists/*

# Install python deps
RUN python3 -m pip install \
  coverage \
  lxml \
  nose \
  numpy \
  pylint

ADD rclone.conf /root/.config/rclone/

ADD itk_gcc_check.diff /opt

ENV IMAGE_NAME=focal

COPY --from=builder /tmp/cppcheck-${CPPCHECK_VERSION}/cppcheck /usr/bin/
COPY --from=builder /tmp/cppcheck-${CPPCHECK_VERSION}/cfg /usr/share/cppcheck/cfg
COPY --from=builder /tmp/cppcheck-${CPPCHECK_VERSION}/addons/*.py /usr/bin/
COPY --from=builder /tmp/cppcheck-${CPPCHECK_VERSION}/addons/*/*.py /usr/bin/

# NB: Zip archive name is sonar-scanner-cli-x.y.z.nnnn-linux.zip
#     but created dir is  sonar-scanner-x.y.z.nnnn-linux (i.e. without "-cli" keyword)

RUN cd /usr/local \
    && curl -L -o ./sonar-scanner.zip \
         https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip \
    && unzip sonar-scanner.zip \
    && rm sonar-scanner.zip \
    && mv sonar-scanner-${SONAR_SCANNER_VERSION}-linux sonar-scanner

ENV SONAR_RUNNER_HOME=/usr/local/sonar-scanner

SHELL ["/bin/bash", "-c"]

# Install OTB
RUN cd /opt \
 && wget -nv https://www.orfeo-toolbox.org/packages/archives/OTB/OTB-7.4.1-Linux64.run \
 && chmod +x OTB-7.4.1-Linux64.run \
 && ./OTB-7.4.1-Linux64.run --target /opt/otb \
# && patch -p1 < itk_gcc_check.diff \
 && source /opt/otb/otbenv.profile \
 && ctest -S /opt/otb/share/otb/swig/build_wrapping.cmake -VV \
 && rm OTB-7.4.1-Linux64.run

ENV LD_LIBRARY_PATH=/opt/otb/lib \
    OTB_APPLICATION_PATH=/opt/otb/lib/otb/applications \
    PATH=$PATH:/opt/otb/bin:/usr/local/sonar-scanner/bin \
    PYTHONPATH=/opt/otb/lib/python \
    GDAL_DATA=/opt/otb/share/gdal \
    PROJ_LIB=/opt/otb/share/proj \
    GEOTIFF_CSV=/opt/otb/share/epsg_csv

# ENTRYPOINT ["/bin/bash", "-c"]
