# WASP - Weighted Average Synthesis Processor

Image processing chain based on OTB to create monthly syntheses of cloud-free reflectances for Sentinel-2 or Venus Level-2A products distributed by the [Theia Land data centre](https://theia.cnes.fr/atdistrib/rocket/#/home).

<a href="Package/wasp.jpg"><img  title="Monthly synthesis over France" src="Package/wasp.jpg" alt="" width="500" height="400" align="middle"  /></a>

## Inputs, algorithm and outputs
The processing chain creates a synthesis image from multiple Level-2A tiles produced with [MAJA](https://labo.obs-mip.fr/multitemp/maccs-how-it-works/). MAJA Level 2A products can be obtained:

* [From Theia, the French Land data centre, for several countries or regions](https://theia.cnes.fr/atdistrib/rocket/#/search?page=1&collection=SENTINEL2&processingLevel=LEVEL2A)
* [From PEPS, as an on-demand processing, anywhere on the world](https://github.com/olivierhagolle/maja_peps)
* [Using MAJA on user side](https://gitlab.orfeo-toolbox.org/maja/maja/)

The weigted average synthesis method to combine the inputs into one single synthesis is described [here](https://labo.obs-mip.fr/multitemp/theias-sentinel-2-l3a-monthly-cloud-free-syntheses/).
Once synthesized, a Level-3 product is generated further described in the [format description](https://labo.obs-mip.fr/multitemp/theias-l3a-product-format/). Analysis-Ready Level-3 products can be obtained from the following locations:

* Theia produces [Level 3A products with WASP over France, Italy, Spain and other selected regions](https://theia.cnes.fr/atdistrib/rocket/#/search?page=1&collection=SENTINEL2&processingLevel=LEVEL3A)
* DLR runs WASP over Germany and adjacent S2 tiles, you can access it [here](https://code-de.org/en/portfolio/?id=19)

Should you want to produce your own Level-3 products, follow the instructions on how to install WASP below.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. MAJA is available as pre-compiled binary, or as a source code that you will have to compile.


### Pre-compiled binaries

WASP binaries are available for Linux distributions [**under the following link**](https://logiciels.cnes.fr/en/node/128?type=tel). Develop versions are accessible [here](https://gitlab.orfeo-toolbox.org/remote_modules/wasp/-/jobs).

To install it, first make sure you have GLU installed:

__For CentOS__

```
yum install mesa-libGLU-devel
```

__For Ubuntu__
```
apt-get install libglu1-mesa-dev
```

Then extract and execute the downloaded executable:

```
./WASP-x.x.run --target /path/to/install
```

### Compiling from source

#### Prerequisites

The program has the following dependencies:
* OTB 7.4 (https://orfeo-toolbox.org)
* Python >3.6 (https://www.python.org/downloads/)
* CMake >3.10.2 (https://cmake.org)
* GCC >6.3.0 (https://gcc.gnu.org) or any other C/C++ compiler for your system

In order to install the software, first install CMake, the c/c++ compiler and the python interpreter (optional). Then follow the tutorial on how to install OTB via the Superbuild to the desired location:

*https://www.orfeo-toolbox.org/SoftwareGuide/SoftwareGuidech2.html#x16-220002.1.2*

Note: You do not need to activate/deactivate any modules. The default configuration works out of the box.

#### compile and install WASP:
```
cd $WASP_DIR
mkdir build && install
cd build
cmake .. -DCMAKE_PREFIX_PATH=/path/to/OTB
```

Eventually you have to specify the location to your compiler by setting ```CMAKE_C_COMPILER``` and ```CMAKE_CXX_COMPILER```. `-DCMAKE_INSTALL_PREFIX=/path/to/your/binaries` might also help.
Then, run
```
make && make install
```

## Run the software

In order to run the processing chain, the script WASP located in the bin/ folder after installation has to be called.
For an explanation on which inputs the program takes, simply execute

```
./bin/WASP --help
```

You will need the python interpreter within the same directory. Should it be located elsewhere, but still within your `PATH`, then run:

```
python ./bin/WASP.py --help
```

### Example

First, you need to download and unzip the Level-2 products of your choice. Have a look at the [theia_download tool](https://github.com/olivierhagolle/theia_download) if you don't know how to do this.
The following command line creates a composite centered around the 2018/05/02 out of 9 Level-2A products from a 46-day period:

```
./bin/WASP \
--input ./SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-
7/SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180518-104024-461_L2A_T32ULV_D_V1-7/SENTINEL2A_20180518-104024-
461_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180508-104025-460_L2A_T32ULV_D_V1-7/SENTINEL2A_20180508-104025-
460_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180515-103024-459_L2A_T32ULV_D_V1-7/SENTINEL2A_20180515-103024-
459_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180505-103125-791_L2A_T32ULV_D_V1-7/SENTINEL2A_20180505-103125-
791_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180415-103544-548_L2A_T32ULV_D_V1-7/SENTINEL2A_20180415-103544-
548_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2B_20180420-103302-810_L2A_T32ULV_D_V1-7/SENTINEL2B_20180420-103302-
810_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180418-104512-083_L2A_T32ULV_D_V1-7/SENTINEL2A_20180418-104512-
083_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180412-105510-678_L2A_T32ULV_D_V1-7/SENTINEL2A_20180412-105510-
678_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
--out . \
--date 20180502 \
--synthalf 23 \
```

Once the processing finishes, the following line should be displayed:
```
================= WASP synthesis of 9 inputs finished in xxxx.xx s =================
```

The resulting product can be then found under
```
./SENTINEL2X-20180502-000000-000_L3A_T32ULV_C_V1-1
```

Click [here](https://labo.obs-mip.fr/multitemp/theias-l3a-product-format/) for the format description.

### Example with a configuration file

One can also pass the parameters to WASP as a json file, using the -f option, previous example can be called like this:

./bin/WASP -f wasp_params.json

wasp_params.json containing :

```
{
    "input": ["./SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-7/SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180518-104024-461_L2A_T32ULV_D_V1-7/SENTINEL2A_20180518-104024-461_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        ",./SENTINEL2A_20180508-104025-460_L2A_T32ULV_D_V1-7/SENTINEL2A_20180508-104025-460_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180515-103024-459_L2A_T32ULV_D_V1-7/SENTINEL2A_20180515-103024-459_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180505-103125-791_L2A_T32ULV_D_V1-7/SENTINEL2A_20180505-103125-791_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180415-103544-548_L2A_T32ULV_D_V1-7/SENTINEL2A_20180415-103544-548_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2B_20180420-103302-810_L2A_T32ULV_D_V1-7/SENTINEL2B_20180420-103302-810_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180418-104512-083_L2A_T32ULV_D_V1-7/SENTINEL2A_20180418-104512-083_L2A_T32ULV_D_V1-7_MTD_ALL.xml",
        "./SENTINEL2A_20180412-105510-678_L2A_T32ULV_D_V1-7/SENTINEL2A_20180412-105510-678_L2A_T32ULV_D_V1-7_MTD_ALL.xml"],
    "out": ".",
    "date": "20180502",
    "synthalf": 23
}
```

## Light mode

The light mode is available ony for Sentinel 2 products, it allow to obtain R3 products with a resolution of 60m. Additional parameters are
created specifically for this mode: lightmode, bands and writedts.

- lightmode : activate the lightmode
- bands : list of bands to keep in numerical order (B4 and B8 mandatory for NDVI computation)
- writedts : write DTS mask in the output product

### Example with light mode

```
./bin/WASP \
--input ./SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-
7/SENTINEL2A_20180525-103024-462_L2A_T32ULV_D_V1-7_MTD_ALL.xml \
./SENTINEL2A_20180518-104024-461_L2A_T32ULV_D_V1-7/SENTINEL2A_20180518-104024-
461_L2A_T32ULV_D_V1-7_MTD_ALL.xml
--out . \
--lightmode True
--bands B4,B8,B11,B12
--writedts True

```

## Built With

* [OTB](https://orfeo-toolbox.org) - The Orfeo toolbox

## History/Copyright

WASP method was initially developped at CESBIO by O.Hagolle for the VENµS satellite.
WASP processor comes from the initial work performed within the [Sen2Agri](http://www.esa-sen2agri.org/) project, by the following consortium :

* **Université Catholique de Louvain (UCL)**
* **CS Romania**
* **CS France**
* **Centre d'Etudes Spatiales de la Biosphère (CESBIO)**

Within Sen2Agri, WASP processor was mainly developped at CS-Romania by :
Cosmin Udroiu, Alex Grosu, Laurentiu Nicola, Lucian Barbulescu and Anca Trasca

WASP was then adapted to CNES context by Peter Kettig from
* **Centre National d'Etudes Spatiales (CNES)** 

## License

This project is licensed under the GPLv3+ License - see the [LICENSE.md](LICENSE.md) file for details

